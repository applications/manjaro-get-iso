## Download and verify a Manjaro ISO.

The tool is a command line tool limited to x86 ISO; providing a convenient way of downloading and verifying an ISO as they are listed on https://manjaro.org/download.

Neither Sway nor ARM is supported, sway because the files is located elsewhere and ARM because there is no signature.

Besides the official downloads, the script has options to fetch the latest stable [release review] or unstable developer previews.

## Get started
Besides the basic Python modules - the script relies on the [Python requests][5] module and [p7zip]. You can check if you have p7zip installed using

    which 7z

### Manjaro
On Manjaro you don't need to install the requests module - it is present as a dependency of pacman-mirrors.

You may need to install p7zip depending on the result of above check

    sudo pacman -Syu p7zip

### Other Linux
If you are using another Linux you can use requirements.txt to install the necessary dependency.

You will also need to fetch the public key for Manjaro Build Server

    gpg --recv-keys 3B794DE6D4320FCE594F4171279E7CF5D8D56EC8

Use your systems package manager to install p7zip.

### Setup
Create the folder **~/.local/bin**

    mkdir -p ~/.local/bin

Then create a new file in this bin folder - name the file **get-iso** - then use your favorite text editor to copy paste [the code][1] into the new file.

Make the file executable

    chmod +x ~/.local/bin/get-iso

## Usage
```
 $ get-iso -h
usage: get-iso [-h] [-f] [-o [OUT_DIR]] [-r | -p] {plasma,xfce,gnome,cinnamon,i3}

This tool will download a named Manjaro ISO

positional arguments:
  {plasma,xfce,gnome,cinnamon,i3}
                        Edition e.g. plasma or xfce.

options:
  -h, --help            show this help message and exit
  -f, --full            Download full ISO
  -o [OUT_DIR], --out-dir [OUT_DIR]
                        Folder to store dowloaded ISO files.

Previews:
  -r, --review          Get Latest Release Review ISO
  -p, --preview         Get Latest Developer Preview ISO

get-iso v. 0.9 - GPL v3 or later <https://www.gnu.org/licenses/gpl.html>

```
### Examples
The script defaults to pull the minimal ISO and downloaded files is placed your home folder. 


Example downloading **minimal plasma**
```
get-iso plasma
```
```
 $ get-iso plasma
Downloading: manjaro-kde-22.1.3-minimal-230529-linux61.iso
Downloading: manjaro-kde-22.1.3-minimal-230529-linux61.iso.sig
Wait for verification ...
gpg: assuming signed data in 'manjaro-kde-22.1.3-minimal-230529-linux61.iso'
gpg: Signature made man 29 maj 2023 11:46:55 CEST
gpg:                using RSA key 3B794DE6D4320FCE594F4171279E7CF5D8D56EC8
gpg: Good signature from "Manjaro Build Server <build@manjaro.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 3B79 4DE6 D432 0FCE 594F  4171 279E 7CF5 D8D5 6EC8
```

## Similar tool
There is a similar tool (without release-review ISO) in the repo [**manjaro-iso-downloader**][6] with a small GUI created using yad.

So if you are the point and click user, you can install it from the repo

    sudo pacman -Syu manjaro-iso-downloader

## Reference
### Official releases
 
- https://manjaro.org/download

### Release reviews

- https://github.com/manjaro/release-review/releases

### Developer previews

- https://github.com/manjaro-gnome/download/releases
- https://github.com/manjaro-plasma/download/releases
- https://github.com/manjaro-xfce/download/releases

[1]: https://gitlab.manjaro.org/applications/manjaro-get-iso/-/blob/development/get-iso
[5]: https://pypi.org/project/requests/
[6]: https://software.manjaro.org/package/manjaro-iso-downloader
[release review]: https://github.com/manjaro/release-review/releases/latest
[p7zip]: https://software.manjaro.org/package/p7zip
